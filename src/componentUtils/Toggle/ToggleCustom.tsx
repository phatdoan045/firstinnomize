import React from 'react';
//import ant design
import { Switch } from 'antd';
// import Ant Design
import { Col } from 'antd';
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';

interface Props {
  classNameComponent: string;
  onChangeToggle: (value: boolean) => void;
  classNameTitle: string;
  titleToggle: string;
}

function ToggleCustom(props: Props) {
  return (
    <>
      <Col span={8} className={props.classNameComponent}>
        <Switch
          checkedChildren={<CheckOutlined />}
          unCheckedChildren={<CloseOutlined />}
          onChange={props.onChangeToggle}
        />
      </Col>
      <Col span={12} className={props.classNameTitle}>
        <p>{props.titleToggle}</p>
      </Col>
    </>
  );
}
export default ToggleCustom;
