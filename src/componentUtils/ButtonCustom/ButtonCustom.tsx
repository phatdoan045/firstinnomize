import React, { Component } from 'react';

// import scss
import './ButtonCustom.scss'

interface Props {
  icon: any;
  title: string;
}
class ButtonCustom extends Component<Props> {
  render() {
    let { icon, title } = this.props;
    return (
      <label className="conponent__button_Custom">
        {icon}
        <span className="conponent__button_Custom--span">{title}</span>
      </label>
    );
  }
}

export default ButtonCustom;
