import React, { Component } from 'react';
// import Ant Design
import { Checkbox } from 'antd';
// Import Redux
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { postsActions } from '../../../store/Post/Post.action';

// import scss
import './PostsList.scss';
import PostsItem from './PostsItem/PostsItem';

interface IProps {
  postsReducers: any;
  actionsPosts: any;
}
interface IState {}
class PostsList extends Component<IProps> {
  componentDidMount() {
    this.props.actionsPosts.getListPostsAPI();
  }
  render() {
    let { postsReducers } = this.props;
    console.log(postsReducers);
    return (
      <>
        <table style={{ width: '100%' }} className="component__postList">
          <thead>
            <tr className="component__postList-row">
              <th>
                <Checkbox />
              </th>
              <th>Id</th>
              <th>Title</th>
              <th>Time</th>
              <th>Com.</th>
              <th>Views</th>
              <th>Author</th>
              <th>Status</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {postsReducers ? (
              postsReducers.posts.map((items: any, index: number) => (
                <PostsItem posts={items} key={index} />
              ))
            ) : (
              <p>No Posts</p>
            )}
          </tbody>
        </table>
      </>
    );
  }
}
export default connect(
  (state: any) => ({
    postsReducers: state.postsReducers,
  }),
  (dispatch: Dispatch) => ({
    actionsPosts: bindActionCreators(postsActions, dispatch),
  })
)(PostsList);
