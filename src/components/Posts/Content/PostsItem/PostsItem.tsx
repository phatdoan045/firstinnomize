import React, { Component } from 'react';

// import Ant Design
import { Checkbox } from 'antd';
// import icon material
import CheckIcon from '@material-ui/icons/Check';
import EditIcon from '@material-ui/icons/Edit';
import VisibilityIcon from '@material-ui/icons/Visibility';
// import scss
import './PostsItem.scss';
import ButtonCustom from '../../../../componentUtils/ButtonCustom/ButtonCustom';

type Post ={
  id: Number;
  title: String;
  datePosts: String;
  myAuthor: String;
  status: Number;
  completed: Boolean;
  views: Number;
}

interface IProps {
  posts: Post;
}

class PostsItem extends Component<IProps> {
  render() {
    let { posts } = this.props;
    return (
      <>
        <tr className="component__postList-row">
          <td>
            <Checkbox />
          </td>
          <td>{posts.id}</td>
          <td>{posts.title}</td>
          <td>{posts.datePosts}</td>
          <td>{posts.completed ? <CheckIcon /> : <p>No</p>}</td>
          <td>{posts.views}</td>
          <td>{posts.myAuthor}</td>
          <td className="component__postList-status">{!posts.status ? "Draft" : "Published" }</td>
          <td>
                <ButtonCustom icon={<EditIcon/>} title="Edit" />
                <ButtonCustom icon={<VisibilityIcon/>} title="Show" />
          </td>
        </tr>
      </>
    );
  }
}

export default PostsItem;
