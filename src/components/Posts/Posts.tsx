import React, { Component } from 'react';

// import Ant Design
import { Row, Col } from 'antd';

// import scss
import './Posts.scss';
// Import Redux
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { postsActions } from '../../store/Post/Post.action';

import FilterSearch from './Filters/Search/Search';
import PostsList from './Content/PostsList';
import FilterStatus from './Filters/Status/Status';
import ToggleCustom from '../../componentUtils/Toggle/ToggleCustom';

interface IProps {
  actionsPosts: any;
}
class Posts extends Component<IProps> {
  onChangeMyPosts = (checked: boolean) => {
    if (checked) this.props.actionsPosts.FiltersMyPosts('Innomize');
    else {
      this.props.actionsPosts.FiltersMyPostsNoCheck();
    }
  };
  onChangeCompleted= (checked:boolean)=>{
    if (checked) this.props.actionsPosts.FiltersCompleted();
    else {
      this.props.actionsPosts.FiltersCompletedNoCheck("Innomize");
    }
  }
  render() {
    return (
      <>
        <Row>
          <Col span={24} className="layout_Post">
            <Col span={24} className="layout_Post--header">
              <Col span={12}>
                <Row>
                  <Col span={6}>
                    <FilterSearch />
                  </Col>
                  <Col span={6}>
                    <ToggleCustom
                      classNameComponent=""
                      classNameTitle=""
                      onChangeToggle={this.onChangeMyPosts}
                      titleToggle="My Post"
                    />
                  </Col>
                  <Col span={6}>
                  <ToggleCustom
                      classNameComponent=""
                      classNameTitle=""
                      onChangeToggle={this.onChangeCompleted}
                      titleToggle="Completed"
                    />
                  </Col>
                  <Col span={6}>
                    <FilterStatus />
                  </Col>
                </Row>
              </Col>
              <Col span={12}></Col>
            </Col>
            <Col span={24} className="layout_Post--content">
              <PostsList />
            </Col>

            <Col span={24} className="layout_Post--footer"></Col>
          </Col>
        </Row>
      </>
    );
  }
}
export default connect(null, (dispatch: Dispatch) => ({
  actionsPosts: bindActionCreators(postsActions, dispatch),
}))(Posts);
