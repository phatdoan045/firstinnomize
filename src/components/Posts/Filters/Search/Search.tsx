import React, { Component } from 'react';

//import icon 
import SearchIcon from '@material-ui/icons/Search';

// import scss
import './Search.scss'

class Search extends Component {
    render() {
        return (
            <> 
              <div className="conponent__filters">
                <input className="conponent__filters-input" placeholder="Search"/>
                <span className="conponent__filters-icon"><SearchIcon/></span>
              </div>
             
            </>
        );
    }
}

export default Search;