import React, { Component } from 'react';
//
import { Select, Row, Col } from 'antd';
// Import Redux
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { postsActions } from '../../../../store/Post/Post.action';
// import scss
import './Status.scss';
const { Option } = Select;

interface IProps {
  actionsPosts: any;
}
class Status extends Component<IProps> {
  handleChangeStatus = (value: number) => {
    if (value !== 2) this.props.actionsPosts.FilterStatus(value,"Innomize");
    else this.props.actionsPosts.FilterStatusNoCheck("Innomize");
  };
  render() {
    return (
      <>
        <Row className="component__Filters-Status">
          <Col span={12} className="component__Filters-Status-Select">
            <Select style={{ width: '100%' }} onChange={this.handleChangeStatus}>
              <Option value={2}>Select All</Option>
              <Option value={0}>Draft</Option>
              <Option value={1}>Published</Option>
            </Select>
          </Col>
          <Col span={8} className="component__Filters-Status-Title">
            <p>Status</p>
          </Col>
        </Row>
      </>
    );
  }
}
export default connect(null, (dispatch: Dispatch) => ({
  actionsPosts: bindActionCreators(postsActions, dispatch),
}))(Status);
