import * as types from './Post.constant';
import API from '../../services/api.service';

export const getListPostsAPI = () => {
  return async (dispatch: any) => {
    try {
      const result = await API.get({ endpoint: `posts` });
      return dispatch(getListPostsSuccess(result.data));
    } catch (e) {
      dispatch(getListPostsFaile());
    }
  };
};

export const getListPostsSuccess = (listPosts: any) => {
  return {
    type: types.GET_POSTS_SUCCSESS,
    listPosts,
  };
};

export const getListPostsFaile = () => {
  alert("Don't connect Server");
  return {
    type: types.GET_POSTS_FAILE,
  };
};

export const FiltersMyPosts = (nameMyPost: string) => {
  return {
    type: types.FILTERS_MYPOSTS,
    nameMyPost,
  };
};

export const FiltersMyPostsNoCheck = () => {
  return {
    type: types.FILTERS_MYPOSTS_NO_CHECK,
  };
};

export const FiltersCompleted = () => {
  return {
    type: types.FILTERS_COMPLETED,
  };
};

export const FiltersCompletedNoCheck = (myPost: String) => {
  return {
    type: types.FILTERS_COMPLETED_NO_CHECK,
    myPost,
  };
};

export const FilterStatus = (valueStatus:number, myAuthor:string) => {
    return {
      type: types.FILTERS_STATUS,
      valueStatus,
      myAuthor
    };
  };
  
export const FilterStatusNoCheck = ( myAuthor:string) => {
    return {
      type: types.FILTERS_STATUS_NO_CHECK,
      myAuthor
    };
};

export const postsActions = {
  getListPostsAPI,
  FiltersMyPosts,
  FiltersMyPostsNoCheck,
  FiltersCompleted,
  FiltersCompletedNoCheck,
  FilterStatus,
  FilterStatusNoCheck
};
