import * as types from '../Post/Post.constant';

const initialState: any = {
  posts: [],
  postsFlag: [],
  checkMyPost: false,
  checkCompleted: false,
  valueStatus: 2,
  totalPage: 0,
};

const postsReducers = (state = initialState, action: any) => {
  switch (action.type) {
    case types.GET_POSTS_SUCCSESS:
      state.posts = action.listPosts;
      state.postsFlag = action.listPosts;
      return { ...state };
    case types.FILTERS_MYPOSTS:
      state.checkMyPost = true;
      state.posts = state.posts.filter((posts: any) => posts.myAuthor === action.nameMyPost);
      return { ...state };
    case types.FILTERS_MYPOSTS_NO_CHECK:
      state.checkMyPost = false;
      if (state.checkCompleted && state.valueStatus !== 2) {
        state.posts = state.postsFlag.filter(
          (posts: any) => posts.completed === true && posts.status === state.valueStatus,
        );
      } else if (state.checkCompleted) {
        state.posts = state.postsFlag.filter((posts: any) => posts.completed === true);
      } else if (state.valueStatus !== 2) {
        state.posts = state.postsFlag.filter((posts: any) => posts.status === state.valueStatus);
      } else {
        state.posts = state.postsFlag;
      }
      return { ...state };
    case types.FILTERS_COMPLETED:
      state.checkCompleted = true;
      state.posts = state.posts.filter((posts: any) => posts.completed === true);
      return { ...state };
    case types.FILTERS_COMPLETED_NO_CHECK:
      state.checkCompleted = false;
      if (state.checkMyPost && state.valueStatus !== 2) {
        state.posts = state.postsFlag.filter(
          (posts: any) => posts.myAuthor === action.myPost && posts.status === state.valueStatus,
        );
      } else if (state.checkMyPost) {
        state.posts = state.postsFlag.filter((posts: any) => posts.myAuthor === action.myPost);
      } else if (state.valueStatus !== 2) {
        state.posts = state.postsFlag.filter((posts: any) => posts.status === state.valueStatus);
      } else {
        state.posts = state.postsFlag;
      }
      return { ...state };
    case types.FILTERS_STATUS:
      state.valueStatus = action.valueStatus;
      if (state.checkMyPost && state.checkCompleted) {
        state.posts = state.postsFlag.filter(
          (posts: any) =>
            posts.myAuthor === action.myAuthor &&
            posts.completed === true &&
            posts.status === action.valueStatus,
        );
      } else if (state.checkMyPost) {
        state.posts = state.postsFlag.filter(
          (posts: any) => posts.myAuthor === action.myAuthor && posts.status === action.valueStatus,
        );
      } else if (state.checkCompleted) {
        state.posts = state.postsFlag.filter(
          (posts: any) => posts.completed === true && posts.status === action.valueStatus,
        );
      }else{
        state.posts = state.postsFlag.filter(
          (posts: any) => posts.status === action.valueStatus
        );
      }
      return { ...state };
    case types.FILTERS_STATUS_NO_CHECK:
      state.valueStatus = 2;
      if (state.checkMyPost && state.checkCompleted) {
        state.posts = state.postsFlag.filter(
          (posts: any) => posts.myAuthor === action.myAuthor && posts.completed === true,
        );
      } else if (state.checkMyPost) {
        state.posts = state.postsFlag.filter((posts: any) => posts.myAuthor === action.myAuthor);
      } else if (state.checkCompleted) {
        state.posts = state.postsFlag.filter((posts: any) => posts.completed === true);
      } else {
        state.posts = state.postsFlag;
      }
      return { ...state };
    default:
      return state;
  }
};
export default postsReducers;
