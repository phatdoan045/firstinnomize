import { combineReducers } from "redux";
import postsReducers from './ListPosts'
const rootReducer = combineReducers({
    postsReducers,
});

export default rootReducer