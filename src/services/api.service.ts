import axios, { AxiosInstance, AxiosResponse } from 'axios';
import { urlServer } from './domain';

class API {
  private api: AxiosInstance;
  constructor() {
    const api = axios.create({
      baseURL: `${urlServer}`,
    });
    this.api = api;
  }

  get(options: { endpoint: string }) {
    return this.api
      .request({
        method: 'GET',
        url: options.endpoint,
        responseType: 'json',
      })
  }
  post(
    options: { endpoint: string },
    data: { data: any },
    callback: (status: number, res: any) => void
  ) {
    return this.api
      .request({
        method: 'POST',
        url: options.endpoint,
        data: data.data,
        responseType: 'json',
        headers: { 'Content-Type': 'multipart/form-data' },
      })
      .then((response: AxiosResponse) =>
        callback(response.status, response.data)
      )
      .catch((error: any) => {
        if (error.response)
          callback(error.response.status, error.response.data);
        else {
          alert('Error!! No connected Server ');
        }
      });
  }
  delete(
    options: { endpoint: string },
    callback: (status: number, res: any) => void
  ) {
    return this.api
      .request({
        method: 'DELETE',
        url: options.endpoint,
        responseType: 'json',
      })
      .then((response: AxiosResponse) =>
        callback(response.status, response.data)
      )
      .catch((error: any) => {
        if (error.response)
          callback(error.response.status, error.response.data);
        else {
          alert('Error!! No connected Server ');
        }
      });
  }
  put(
    options: { endpoint: string },
    data: { data: any },
    callback: (status: number, res: any) => void
  ) {
    return this.api
      .request({
        method: 'PUT',
        url: options.endpoint,
        data: data.data,
        responseType: 'json',
        headers: { 'Content-Type': 'multipart/form-data' },
      })
      .then((response: AxiosResponse) =>
        callback(response.status, response.data)
      )
      .catch((error: any) => {
        if (error.response)
          callback(error.response.status, error.response.data);
        else {
          alert('Error!! No connected Server ');
        }
      });
  }
  postJSon(
    options: { endpoint: string },
    data: { data: any },
    callback: (status: number, res: any) => void
  ) {
    return this.api
      .request({
        method: 'POST',
        url: options.endpoint,
        data: data.data,
        responseType: 'json',
        headers: { 'Content-Type': 'application/json' },
      })
      .then((response: AxiosResponse) =>
        callback(response.status, response.data)
      )
      .catch((error: any) => {
        if (error.response)
          callback(error.response.status, error.response.data);
        else {
          alert('Error!! No connected Server ');
        }
      });
  }
}
export default new API();
