
export type Post ={
  id: Number;
  title: String;
  datePosts: String;
  myAuthor: String;
  status: Number;
  completed: Boolean;
  views: Number;
}
