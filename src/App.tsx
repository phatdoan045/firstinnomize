import React from 'react';
import 'antd/dist/antd.css';
import Posts from './components/Posts/Posts';

function App() {
  return (
    <div className="App">
      <Posts />
    </div>
  );
}

export default App;
